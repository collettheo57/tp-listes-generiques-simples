#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include "lst_elm.h"
#include "lst.h"
#include "outils.h"

void printInteger ( int * i )
{
    assert(i);
    printf("%d",(*i));
}

void rmInteger(int *i)
{
    assert(i);
    free(i);
}

bool cmpInteger ( int * i, int * j ) {
    return (*i) < (*j);
}

void printDouble ( double * d ) {
    printf( "%.2lf", (*d) );
}

void rmDouble ( double * d ) {
    free( d );
}

bool cmpDouble ( double * u, double * v ) {
    return (*u) < (*v);
}