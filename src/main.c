#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions

#include "lst_elm.h"
#include "lst.h"
#include "outils.h"

void listehomoreelle();
void listehomoentiere();

int main () 
{
	int cmpt,a=8,b=4,*ptra=&a,*ptrb=&b;
	double x=5.4,y=3.14,*ptrx=&x,*ptry=&y;
	struct lst_t *L=new_lst();
	struct lst_elm_t *E;

	cons(L , ptra ) ; // un entier
	cons(L , ptrb ) ; // un entier
	cons(L , ptrx ) ; // un réel
	cons(L , ptry ) ; // un réel

	//La liste vaut [3.14 ; 5.4 ; 4 ; 8 ] 
	for (cmpt=0,E=getHead(L);cmpt<getNumelm(L);cmpt+=1,E=getSuc(E)) 
		{
			if ( cmpt < 2)
			{
				double *d=(double*)getDatum(E);
				printDouble(d);
			} 
			else 
			{
				int *d=(int*)getDatum(E);
				printInteger(d);
			}
		}

	return EXIT_SUCCESS;
}

void listehomoreelle()
{
	struct lst_t *L=new_lst();
	double *v;

	do{
		double u;

		printf("Entrez un réel (O pour s'arrêter): ");
		scanf("%lf",&u);
		if (u==0)break;

		v=(double*)calloc(1,sizeof(double));
		*v=u;
		insert_ordered(L,v,&cmpDouble);
	}	while(true);
	print_lst(L,&printDouble);
	del_lst(&L,&rmDouble);
}

void listehomoentiere()
{
	struct lst_t *L=new_lst();
	int *v;

	do{
		int u;

		printf("Entrez un entier (O pour s'arrêter): ");
		scanf("%d",&u);
		if (u==0)break;

		v=(int*)calloc(1,sizeof(int));
		*v=u;

		insert_ordered(L,v,&cmpInteger);
	}	while(true);
	print_lst(L,&printInteger);
	struct lst_t *copyL=copy_lst(L);
	print_lst(copyL,&printInteger);
	del_lst(&L,&rmInteger);
	del_lst(&copyL,&rmInteger);
}